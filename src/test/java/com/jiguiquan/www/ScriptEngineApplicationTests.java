package com.jiguiquan.www;

import delight.nashornsandbox.NashornSandbox;
import delight.nashornsandbox.NashornSandboxes;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.script.*;
import java.util.concurrent.Executors;

@SpringBootTest
class ScriptEngineApplicationTests {

    @Test
    void contextLoads() {
    }

    // 简单获取nashorn执行引擎
    @Test
    protected void useNashorn() throws ScriptException {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        System.out.println("------列出Java 中有哪些脚本引擎-------");
        scriptEngineManager.getEngineFactories().forEach(f -> System.out.println(f.getNames()));

        System.out.println("-------执行一个简单的js脚本--------");
        ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");
        nashorn.eval("print('hello world')");
    }

    // 测试JAVA传参调用JS，再由JS传参调用JAVA
    public static final String JS_STR1 =
            "var MyJavaClass = Java.type('com.jiguiquan.www.controller.ForJsCall');\n" +
            "var result = MyJavaClass.jsCall(name);\n" +
            "print(result);\n";

    @Test
    protected void callJavaFromJs() throws ScriptException {
        ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");
        SimpleBindings simpleBindings = new SimpleBindings();
        simpleBindings.put("name", "吉桂权");
        nashorn.eval(JS_STR1, simpleBindings);
    }

    @Test
    public void test1() throws ScriptException {
        String jsStr2 = "function test(){return 'Hello World'}; ";
        ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");
        System.out.println(nashorn.eval(jsStr2));
    }

    @Test
    public void test2() throws ScriptException, NoSuchMethodException {
        String jsStr1 = "function test(){return 'Hello World'};";
        ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");

        nashorn.eval(jsStr1);

        // 将nashorn转为Invocable，以执行它的invokeFunction指定方法接口
        System.out.println(((Invocable) nashorn).invokeFunction("test"));
    }

    @Test
    public void testSandbox() throws ScriptException {
        NashornSandbox sandbox = NashornSandboxes.create();
        sandbox.setMaxCPUTime(100);// 设置脚本执行允许的最大CPU时间（以毫秒为单位），超过则会报异常,防止死循环脚本
        sandbox.setMaxMemory(1024 * 1024); //设置JS执行程序线程可以分配的最大内存（以字节为单位），超过会报ScriptMemoryAbuseException错误
        sandbox.allowNoBraces(false); // 是否不允许使用大括号
        sandbox.allowLoadFunctions(true); // 是否允许nashorn加载全局函数
        sandbox.setMaxPreparedStatements(30); // because preparing scripts for execution is expensive // LRU初缓存的初始化大小，默认为0
        sandbox.setExecutor(Executors.newSingleThreadExecutor());// 指定执行程序服务，该服务用于在CPU时间运行脚本

        String jsStr = "function test(){return 'Hello SandBox'}; test()";
        System.out.println(sandbox.eval(jsStr));
    }

    @Test
    public void testSandbox2() throws ScriptException, NoSuchMethodException {
        NashornSandbox sandbox = NashornSandboxes.create();
        sandbox.setMaxCPUTime(100);// 设置脚本执行允许的最大CPU时间（以毫秒为单位），超过则会报异常,防止死循环脚本
        sandbox.setMaxMemory(1024 * 1024); //设置JS执行程序线程可以分配的最大内存（以字节为单位），超过会报ScriptMemoryAbuseException错误
        sandbox.allowNoBraces(false); // 是否不允许使用大括号
        sandbox.allowLoadFunctions(true); // 是否允许nashorn加载全局函数
        sandbox.setMaxPreparedStatements(30); // because preparing scripts for execution is expensive // LRU初缓存的初始化大小，默认为0
        sandbox.setExecutor(Executors.newSingleThreadExecutor());// 指定执行程序服务，该服务用于在CPU时间运行脚本

        String jsStr = "function test(){return 'Hello SandBox'};";
        sandbox.eval(jsStr);
        System.out.println(sandbox.getSandboxedInvocable().invokeFunction("test"));
    }

    public static final String invokeJs = "function test(a, b, c){return a + b + c}";

    // 使用nashorn执行js函数
    @Test
    public void testInvoke1() throws ScriptException, NoSuchMethodException {
        ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");
        nashorn.eval(invokeJs);
        System.out.println(((Invocable) nashorn).invokeFunction("test", 1, 2, 3));
    }

    // 使用sandbox执行js函数
    @Test
    public void testInvoke2() throws ScriptException, NoSuchMethodException {
        NashornSandbox sandbox = NashornSandboxes.create();
        sandbox.setMaxCPUTime(100);
        sandbox.setMaxMemory(1024 * 1024);
        sandbox.allowNoBraces(false);
        sandbox.allowLoadFunctions(true);
        sandbox.setMaxPreparedStatements(30);
        sandbox.setExecutor(Executors.newSingleThreadExecutor());
        sandbox.eval(invokeJs);
        System.out.println((sandbox.getSandboxedInvocable().invokeFunction("test", 1, 2, 3)));
    }
}
