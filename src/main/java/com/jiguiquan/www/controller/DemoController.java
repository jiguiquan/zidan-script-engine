package com.jiguiquan.www.controller;

import com.jiguiquan.starter.common.vo.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author jiguiquan
 * @Email jiguiquan@haier.com
 * @Data 2023-06-30 10:06
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/test")
    public BaseResponse<String> demo(){
        return BaseResponse.success("调用成功");
    }
}
