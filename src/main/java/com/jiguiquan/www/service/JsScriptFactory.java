package com.jiguiquan.www.service;

/**
 * @Author jiguiquan
 * @Email jiguiquan@haier.com
 * @Data 2023-06-30 14:18
 */
public class JsScriptFactory {
    public static final String firstP = "m";
    public static final String secondP = "n";
    public static final String STATIC_FUNCTION_NAME = "ruleNodeFunc";

    private static final String JS_WRAPPER_PREFIX_TEMPLATE = "function %s(x, y) { return %s(x, y); function %s(%s, %s) {";
    private static final String JS_WRAPPER_SUFFIX = "}}";

    public static String generateJsScript(String functionName, String scriptBody) {
        String jsWrapperPrefix = String.format(JS_WRAPPER_PREFIX_TEMPLATE, functionName,
                STATIC_FUNCTION_NAME, STATIC_FUNCTION_NAME, firstP, secondP);
        return jsWrapperPrefix + scriptBody + JS_WRAPPER_SUFFIX;
    }
}
