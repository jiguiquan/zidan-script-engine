package com.jiguiquan.www.service;

import com.google.common.util.concurrent.ListenableFuture;

import javax.script.ScriptException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @Author jiguiquan
 * @Email jiguiquan@haier.com
 * @Data 2023-06-30 13:50
 */
public interface JsInvokeService {

    UUID eval(String scriptBody) throws ScriptException;

    Object invokeFunction(UUID scriptId, Object... args) throws ScriptException, NoSuchMethodException;

    void release(UUID scriptId) throws ScriptException;
}
